package facci.pm.realmdb;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

import io.realm.OrderedCollectionChangeSet;
import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

public class MainActivity extends AppCompatActivity {

    Realm uiThreadRealm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Realm.init(this); // context, usually an Activity or Application

        //Open a Realm
        String realmName = "My Project";
        RealmConfiguration config = new RealmConfiguration.Builder().name(realmName).build();
        uiThreadRealm = Realm.getInstance(config);

        addChangeListenerToRealm(uiThreadRealm);

        FutureTask<String> Task = new FutureTask(new BackgroundQuickStart(), "test");
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        executorService.execute(Task);
    }

    //adjuntamos un elemento personalizado OrderedRealmCollectionChangeListener con el addChangeListener()
    private void addChangeListenerToRealm(Realm realm) {
        // all Tasks in the realm
        RealmResults<Task> accessories = uiThreadRealm.where(Task.class).findAllAsync();
        accessories.addChangeListener(new OrderedRealmCollectionChangeListener<RealmResults<Task>>() {
            @Override
            public void onChange(RealmResults<Task> collection, OrderedCollectionChangeSet changeSet) {
                // process deletions in reverse order if maintaining parallel data structures so indices don't change as you iterate
                OrderedCollectionChangeSet.Range[] deletions = changeSet.getDeletionRanges();
                for (OrderedCollectionChangeSet.Range range : deletions) {
                    Log.e("QUICKSTART", "Deleted range: " + range.startIndex + " to " + (range.startIndex + range.length - 1));
                }

                OrderedCollectionChangeSet.Range[] insertions = changeSet.getInsertionRanges();
                for (OrderedCollectionChangeSet.Range range : insertions) {
                    Log.e("QUICKSTART", "Inserted range: " + range.startIndex + " to " + (range.startIndex + range.length - 1));
                }

                OrderedCollectionChangeSet.Range[] modifications = changeSet.getChangeRanges();
                for (OrderedCollectionChangeSet.Range range : modifications) {
                    Log.e("QUICKSTART", "Updated range: " + range.startIndex + " to " + (range.startIndex + range.length - 1));
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // the ui thread realm uses asynchronous transactions, so we can only safely close the realm
        // when the activity ends and we can safely assume that those transactions have completed
        uiThreadRealm.close();
    }

    public class BackgroundQuickStart implements Runnable {
        @Override
        public void run() {
            String realmName = "My Project";
            RealmConfiguration config = new RealmConfiguration.Builder().name(realmName).build();
            Realm backgroundThreadRealm = Realm.getInstance(config);

            //To create a new Task, instantiate an instance of the Task class and add it to the realm in a write block
            //se crean objetos
            Task task1 = new Task("Instrumento", "Piano", "3", "300");
            Task task2 = new Task("Cable", "Cable Jack 3.5mm", "5", "5");
            Task task3 = new Task("Herramienta", "Afinador de guitarra", "3", "20");

            //transacción
            backgroundThreadRealm.executeTransaction (transactionRealm -> {
                transactionRealm.insert(task1);
                transactionRealm.insert(task2);
                transactionRealm.insert(task3);
            });

            //You can retrieve a live collection of all items in the realm
            // all Tasks in the realm
            //buscamos los elementos que hay en la tabla, trae una lista de objetos de tareas
            RealmResults<Task> accessories = backgroundThreadRealm.where(Task.class).findAll();

            //MODIFICAR
            //To MODIFY a task, update its properties in a write transaction block
            Task otherTask = accessories.get(0);
            // all modifications to a realm must happen inside of a write block
            backgroundThreadRealm.executeTransaction( transactionRealm -> {
                //se busca en específico el objeto que hace referencia
                Task innerOtherTask = transactionRealm.where(Task.class)
                        .equalTo("name", otherTask.getName())
                        .equalTo("description", otherTask.getDescription())
                        .equalTo("quantity", otherTask.getQuantity())
                        .equalTo("price", otherTask.getPrice())
                        .findFirst();

                //para modificar se utiliza los metodos relacionados  a la encapsulacion de cada uno de sus campos
                innerOtherTask.setStatus(TaskStatus.Open);
            });

            //ELIMINAR
            //you can DELETE a task by calling the deleteFromRealm() method in a write transaction block:
            Task yetAnotherTask = accessories.get(0);
            String yetAnotherAccessorieName = yetAnotherTask.getName();
            String yetAnotherAccessorieDescription = yetAnotherTask.getDescription();
            String yetAnotherAccessorieQuantity = yetAnotherTask.getQuantity();
            String yetAnotherAccessoriePrice = yetAnotherTask.getPrice();
//            // all modifications to a realm must happen inside of a write block
            backgroundThreadRealm.executeTransaction( transactionRealm -> {
                Task innerYetAnotherTask = transactionRealm.where(Task.class)
                        .equalTo("_id", yetAnotherAccessorieName)
                        .equalTo("_id", yetAnotherAccessorieDescription)
                        .equalTo("_id", yetAnotherAccessorieQuantity)
                        .equalTo("_id", yetAnotherAccessoriePrice)
                        .findFirst();
                innerYetAnotherTask.deleteFromRealm(); //para eliminar usa el método deleteFromRealm
            });

            for (Task task : accessories
            ) {
                Log.e("Accessorie", "Name: " + task.getName()
                        + " Status: " + task.getStatus()
                        + " Description: " + task.getDescription()
                        + " Quantity: " + task.getQuantity()
                        +  "Price: " + task.getPrice());
            }

            // you can also filter a collection
            //otros ejemplos de búsqueda
            // el name empieze con la letra R
            RealmResults<Task> accessoriesThatBeginWithR = accessories.where().beginsWith("name", "R").findAll();
            //con estado abierto
            RealmResults<Task> openAccessories = accessories.where().equalTo("status", TaskStatus.Open.name()).findAll();

            // because this background thread uses synchronous realm transactions, at this point all
            // transactions have completed and we can safely close the realm
            //cerramos la conexion con respecto a la bdd
            backgroundThreadRealm.close();
        }
    }
}